<?php

namespace Drupal\simple_address\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'simple_address_widget' widget.
 *
 * @FieldWidget(
 *   id = "simple_address_widget",
 *   module = "simple_address",
 *   label = @Translation("Simple Address widget"),
 *   field_types = {
 *     "simple_address"
 *   }
 * )
 */
class AddressWidget extends WidgetBase {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $empty_option = ['' => $this->t('Choose option')];
    $country_options = $empty_option;
    $country_options += $this->getCountryOptions();
    $state_field_id_array = ['state'];
    $state_field_id_array += $element['#field_parents'];
    $state_field_id_array[] = $items->getName();
    $state_field_id_array[] = $delta;
    $state_field_id = Html::getId(implode('-', $state_field_id_array));
    $element['#type'] = 'container';
    $element['#address_widget'] = TRUE;
    $element['country_code'] = [
      '#type' => 'select',
      '#default_value' => $items[$delta]->country_code ?? NULL,
      '#required' => $element['#required'],
      '#title' => $this->t('Country'),
      '#title_display' => $element['#title_display'],
      '#options' => $country_options,
      '#ajax' => [
        'callback' => [$this, 'stateAjaxCallback'],
        'wrapper' => $state_field_id,
        'method' => 'replace',
      ],
    ];

    $country_code = $element['country_code']['#default_value'];

    $state_options = $empty_option;
    $state_options += $this->getStateOptions($country_code);
    $element['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State/province'),
      '#default_value' => $items[$delta]->state ?? NULL,
      '#options' => $state_options,
      '#title_display' => $element['#title_display'],
      '#validated' => TRUE,
      '#wrapper_attributes' => [
        'id' => $state_field_id,
      ],
    ];

    $element['city'] = [
      '#title' => $this->t('City'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#default_value' => $items[$delta]->city ?? NULL,
      '#placeholder' => $this->t('Enter city'),
    ];

    $element['address_line'] = [
      '#title' => $this->t('Address'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#placeholder' => $this->t('Enter street name, house number, apt'),
      '#default_value' => $items[$delta]->address_line ?? NULL,
    ];

    $element['postal_code'] = [
      '#title' => $this->t('Postal code'),
      '#type' => 'textfield',
      '#title_display' => $element['#title_display'],
      '#placeholder' => $this->t('Enter postal code'),
      '#default_value' => $items[$delta]->postal_code ?? NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function stateAjaxCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    $empty_option = ['' => $this->t('Choose option')];
    $state_options = $empty_option;
    $triggering_element = $form_state->getTriggeringElement();

    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    $parents[] = 'state';
    $state_element = NestedArray::getValue($form, $parents);

    $added_state_options = $this->getStateOptions($triggering_element['#value']);
    if (reset($added_state_options) === 'No states associated with this country.' || empty($added_state_options)) {
      $state_options = $this->getStateOptions($triggering_element['#value']);
      if (empty($state_options)) {
        $state_options = $empty_option;
      }
    }
    else {
      $state_element["#required"] = TRUE;
      $state_options += $this->getStateOptions($triggering_element['#value']);
    }

    $state_element['#options'] = $state_options;

    return $state_element;
  }

  /**
   * Construct the countries options.
   */
  private function getCountryOptions(): array {
    // phpcs:disable
    $countries = \Drupal::service('simple_address.address_repository')->getCountries();
    // For some reasons DI doesn't work here with ajax.
    // phpcs:enable
    $options = [];
    foreach ($countries as $alpha_code => $country) {
      $options[$alpha_code] = $country['name'];
    }

    return $options;
  }

  /**
   * Build options for country element.
   *
   * @param string|null $country_code
   *   The country alpha 3 code.
   *
   * @return array
   *   The options array.
   */
  private function getStateOptions(string $country_code = NULL): array {
    $options = [];
    if (!$country_code) {
      return $options;
    }
    // phpcs:disable
    // For some reasons DI doesn't work here with ajax.
    $repository = \Drupal::service('simple_address.address_repository');
    // phpcs:enable
    $states = $repository->findSubDivision($country_code);
    foreach ($states as $state) {
      $options[$state['state_code']] = $state['state'];
    }

    return $options;
  }

}
