<?php

namespace Drupal\simple_address;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Extension\ExtensionPathResolver;

/**
 * Class AddressRepository provide the address repository.
 */
class AddressRepository {

  /**
   * Drupal\Component\Serialization\SerializationInterface definition.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected SerializationInterface $serializationJson;

  /**
   * The current module path.
   *
   * @var string
   */
  protected string $modulePath;

  /**
   * Constructs a new AddressRepository object.
   */
  public function __construct(SerializationInterface $serialization_json,
    ExtensionPathResolver $path_resolver) {
    $this->serializationJson = $serialization_json;
    $this->modulePath = $path_resolver->getPath('module', 'simple_address');
  }

  /**
   * Get countries.
   */
  public function getCountries(): array {
    $countries = &drupal_static(__METHOD__, []);
    if (empty($countries)) {
      $countries_storage = $this->modulePath . '/resources/countries.json';
      $countries = Json::decode(file_get_contents($countries_storage));

      $available_countries = array_filter($countries, function ($value) {
        $selected = FALSE;
        if (isset($value['selected'])) {
          $selected = (bool) $value['selected'];
        }
        return $selected;
      });
      if (!empty($available_countries)) {
        $countries = $available_countries;
      }
    }
    return $countries;
  }

  /**
   * Get state.
   */
  public function getStates(): array {
    $states = &drupal_static(__METHOD__, []);
    if (empty($states)) {
      $states_storage = $this->modulePath . '/resources/state_province.json';
      $states = Json::decode(file_get_contents($states_storage));
    }
    return $states;
  }

  /**
   * Find the locality for a given country.
   */
  public function findSubDivision(string $country_code): array {
    $countries = $this->getCountries();
    $country = $countries[$country_code];
    $states = $this->getStates();
    if (isset($states[$country['name']])) {
      return $states[$country['name']];
    }
    else {
      return [
        [
          'state_code' => $country_code,
          'state' => 'No states associated with this country.',
        ],
      ];
    }
  }

  /**
   * Get file of countries with mapping like alpha-2 => alpha-3 codes.
   */
  public function getCountriesAlpha2Codes() {
    $mapping = &drupal_static(__METHOD__, []);
    if (empty($countries)) {
      $mapping_storage = $this->modulePath . "/resources/countries_alfa_2_3_mapping.json";
      $mapping = Json::decode(file_get_contents($mapping_storage));
    }
    return $mapping;
  }

  /**
   * Generate country alpha-3 code from alpha-2.
   *
   * @param string $country_code
   *   Alpha-2 code.
   *
   * @return string
   *   Alpha-3 code.
   */
  public function getCountryAlpha3Code(string $country_code): string {
    $countries = $this->getCountriesAlpha2Codes();
    return $countries[$country_code];
  }

  /**
   * Generate country human name from alpha-3 code.
   *
   * @param string $country_code
   *   Alpha-3 code.
   *
   * @return string
   *   Country human name.
   */
  public function getCountryHumanName(string $country_code): string {
    $countries = $this->getCountries();
    return $countries[$country_code]['name'];
  }

}
